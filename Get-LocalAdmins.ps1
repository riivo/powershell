Invoke-Command -ComputerName "insertNameHere" -ScriptBlock {
    ([ADSI]("WinNT://$env:COMPUTERNAME,computer")).psbase.children.find("Administrators", "Group").psbase.invoke("members") |
        ForEach-Object {
            $_.GetType().InvokeMember("Name",  "GetProperty",  $null,  $_, $null)
        }
} | ForEach-Object {
    Get-ADObject -LDAPFilter "(samaccountname=$_)" | ForEach-Object {
        if ($_.ObjectClass -eq "user") { $_.Name }
        else { Get-ADGroupMember -Recursive $_ | Select-Object -ExpandProperty name }
    }
}