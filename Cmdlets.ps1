# Define the Account
$account       = "user1".Trim().Split("@")[0].Replace(" ", ".")
$adminAccount  = "admin1"
$userList      = "user1", "user2"
$globalCatalog = "server:3268"

# Mailbox Permissions
Get-MailboxPermission -Identity "$account" | select User, AccessRights | sort User
Add-MailboxPermission -Identity  "$account" -User $adminAccount -AccessRights FullAccess
$userList | ForEach-Object { Add-MailboxPermission -Identity "$account" -User $_ -AccessRights FullAccess }
Remove-MailboxPermission -Identity "$account" -User $adminAccount -AccessRights fullaccess

# Info and Statistics
$account = Get-ADUser -Identity $account -Server $globalCatalog -Properties *
(Get-Recipient "$account").EmailAddresses | select SmtpAddress
Get-MailboxStatistics "$account" | select StorageLimitStatus, TotalItemSize, TotalDeletedItemSize | fl
Get-MailboxFolderStatistics "$account" | sort FolderSize | ft -Property Name, FolderPath, FolderSize, ItemsInFolder, Date
Get-Mailbox "$account" | select UseDatabaseQuotaDefaults, IssueWarningQuota, ProhibitSendQuota, ProhibitSendReceiveQuota | fl
Get-InboxRule -Mailbox "$account"
Get-ADUser $account -Properties * | select -ExpandProperty directReports | foreach { $_.Split(",")[0].Replace("CN=", "") }
Get-DynamicDistributionGroup $account | foreach { Get-Recipient -RecipientPreviewFilter $_.RecipientFilter -OrganizationalUnit $_.RecipientContainer }

# Storage quota
$size = 3GB
Set-Mailbox "$account" -UseDatabaseQuotaDefaults $false
Set-Mailbox "$account" -ProhibitSendReceiveQuota ($size + 1GB)
Set-Mailbox "$account" -ProhibitSendQuota $size
Set-Mailbox "$account" -IssueWarningQuota ($size - 100MB)

# Tracking
$params = @{
    End            = ""
    EventId        = "DELIVER"
    MessageSubject = ""
    Recipients     = $account + "@domain"
    ResultSize     = "unlimited"
    Sender         = ""
    Start          = (Get-Date).AddDays(-1)
}
foreach ($key in $($params.Keys)) { if (-not $params.$key) { $params.Remove($key) } }
$msgs = Get-ExchangeServer | Get-MessageTrackingLog @params
$msgs | select Timestamp, MessageSubject, Sender, {$_.Recipients}, ClientHostname

# File shares
explorer.exe (Get-ADUser $account -Server $globalCatalog -Properties HomeDirectory).HomeDirectory
Get-Acl "\\server\share" |
    Select-Object -ExpandProperty Access |
    Select-Object -ExpandProperty IdentityReference |
    Select-Object -ExpandProperty Value |
    ForEach-Object { $_.Split("\")[1] } |
    Where-Object { $_ -ne "Administrators" }