$uri = "http://evserver/EnterpriseVault/ExchangeArchivingReports.aspx"
$dir = "\\webserver\EV_Reports\"
$count = 5
$fileNames = @()
function Get-UriContent ($uri) {
    $webclient = New-Object System.Net.WebClient
    $webclient.UseDefaultCredentials = $true
    $webclient.Encoding = [System.Text.Encoding]::UTF8
    return $webclient.DownloadString($uri)
}
function Get-HtmlHrefs ($content) {
    $html = New-Object -ComObject "HTMLFile"
    $html.IHTMLDocument2_write($content)
    return $html.IHTMLDocument2_links | ForEach-Object { $_.href -replace "about:"}
}
$serverLinks = Get-HtmlHrefs (Get-UriContent $uri)
$servers = $serverLinks | ForEach-Object { $_.Split("=")[1] }
if (-not (Test-Path $dir)) { New-Item -Type Directory $dir | Out-Null }
$servers | ForEach-Object { if (-not (Test-Path "$dir$_")) { New-Item -Type Directory "$dir$_" | Out-Null } }
foreach ($serverLink in $serverLinks) {
    $reportLinks = Get-HtmlHrefs (Get-UriContent $serverLink) |
        Where-Object { $_ -match "Display(CSV|HTML)Report.*Scheduled.*" } |
        Select-Object -First ($count*2) |
        ForEach-Object { $serverLink.Substring(0,52) + $_ -replace "amp;" } |
        ForEach-Object { if ($_ -match "DisplayHtmlReport") { $_ + "&Detail=1" } else { $_ } }
    foreach ($reportLink in $reportLinks) {
        $fileNames += $reportLink.Split('&')[2].Split('=')[1], "\",
            $reportLink.Split("_")[1], "_", $reportLink.Split("_")[2].Split("&")[0], ".",
            (($reportLink -split "Display")[1] -split "Report")[0].ToLower() -join ""
        "Downloading " + $fileNames[-1] + "..."
        Get-UriContent $reportLink | Out-File -Encoding UTF8 "$dir$($fileNames[-1])"
    }
}
$fileNames | ForEach-Object { "<a href='$_'>$_</a><br>" } | Out-File -Encoding UTF8 ($dir + "index.html")
$fileNames | Out-File -Encoding UTF8 ($dir + "reports.txt")
$servers | ForEach-Object { Get-ChildItem $_ | Select-Object -First ((Get-ChildItem $_).Count-$count*2) | Remove-Item }