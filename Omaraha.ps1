&{foreach ($rating in 1..4) {
  foreach ($product in 1..9) {
    $page = Invoke-WebRequest "https://omaraha.ee/et/invest/stats/2018/?rating_range=$rating&country=1&product=$product&total=true&filter="
    $xirr = (@($page.ParsedHtml.getElementsByTagName("table")[1].rows)[-2].outerText.Replace("%","").Replace(",",".").Split(" ")|?{$_}|measure -A).Average
    [PSCustomObject]@{
      rating  = $rating
      product = $product
      xirr    = [math]::Round($xirr, 2)
    }
  }
}} | Tee-Object -Variable omaraha
$omaraha | Sort-Object xirr