$logsLocation = ""
$dateFormat   = "dd_MM_yyyy"
$today        = @{}
$yesterday    = @{}
$output       = @()
foreach ($row in Import-Csv "${logsLocation}Mailbox_Growth_$(Get-Date -Format $dateFormat).csv") {
    $today[$row.MailboxGuid] = $row.DisplayName, $row.ItemCount, $row.TotalItemSizeMB
}
foreach ($row in Import-Csv "${logsLocation}Mailbox_Growth_$(Get-Date (Get-Date).AddDays(-1) -Format $dateFormat).csv") {
    $yesterday[$row.MailboxGuid] = $row.DisplayName, $row.ItemCount, $row.TotalItemSizeMB
}
$guidsConjunction = Compare-Object $($today.Keys) $($yesterday.Keys) -ExcludeDifferent -IncludeEqual |
    Select-Object -ExpandProperty InputObject
foreach ($guid in $guidsConjunction) {
    $output += New-Object PSObject |
        Add-Member -MemberType NoteProperty -PassThru -Name DisplayName -Value $today[$guid][0] |
        Add-Member -MemberType NoteProperty -PassThru -Name MailboxGuid -Value $guid |
        Add-Member -MemberType NoteProperty -PassThru -Name ItemCountChange -Value ($today[$guid][1] - $yesterday[$guid][1]) |
        Add-Member -MemberType NoteProperty -PassThru -Name TotalItemSizeMBChange -Value ($today[$guid][2] - $yesterday[$guid][2])
    $i ++
    Write-Progress -Activity "Comparing mailbox sizes" -Status "$i out of $($guidsConjunction.count)" -PercentComplete ($i / $guidsConjunction.count * 100)
}
return $output