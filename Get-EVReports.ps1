$uri = "http://<evserver>/EnterpriseVault/ExchangeArchivingReports.aspx"
$credentials = Get-Credential

$serverLinks = Invoke-WebRequest -Uri $uri -Credential $credentials -UseBasicParsing |
    select -ExpandProperty Links |
    select -ExpandProperty href
$servers = $serverLinks | foreach { $_.Split("=")[1] }

if (-not (Test-Path "EV_Reports")) { New-Item -Type Directory "EV_Reports" | Out-Null }
$servers | foreach { if (-not (Test-Path "EV_Reports\$_")) { New-Item -Type Directory "EV_Reports\$_" | Out-Null } }

foreach ($serverLink in $serverLinks) {
    $reportLinks = Invoke-WebRequest -Uri $serverLink -Credential $credentials -UseBasicParsing |
        select -ExpandProperty Links |
        select -ExpandProperty href |
        where { $_ -match "Display(CSV|HTML)Report.*Scheduled.*" } |
        select -First 6 |
        foreach { $_ -replace "amp;" -replace "^", $serverLink.Substring(0,52) } |
        foreach { if ($_ -match "DisplayHtmlReport") { $_ -replace "$", "&Detail=1" } else { $_ } }
    $fileNames = $reportLinks | foreach {
        "EV_Reports\" + $_.Split("&")[2].Split("=")[1] + "\" + $_.Split("_")[1] + "_" + $_.Split("_")[2].Split("&")[0] + "." + (($_ -split "Display")[1] -split "Report")[0].ToLower()
    }
    Start-BitsTransfer -Source $reportLinks -Destination $fileNames -Authentication Negotiate -Credential $credentials -Asynchronous | Out-Null
}

while ($jobs = Get-BitsTransfer) {
    $jobs | where { $_.JobState -eq "Transferred" } | Complete-BitsTransfer
    Start-Sleep 5
}