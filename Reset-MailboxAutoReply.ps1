$uri = ""
Import-PSSession (New-PSSession -ConfigurationName Microsoft.Exchange -ConnectionUri $uri -Authentication Kerberos)
Add-PSSnapin Microsoft.Exchange.Management.PowerShell.E2010
Set-ADServerSettings -ViewEntireForest $true

$logDir       = ""
$servers      = Get-ExchangeServer
$before       = @()
$intermediate = @()
$caughtErrors = @()
$after        = @()

foreach ($server in $servers) {
    if (-not ($mailboxes = Get-Mailbox -Server $server.Name -ResultSize unlimited -RecipientTypeDetails UserMailbox, SharedMailbox)) { continue }
    foreach ($mailbox in $mailboxes) {
        try {
            $oooBefore = Get-MailboxAutoReplyConfiguration $mailbox.DistinguishedName -ErrorAction "Stop"
        }
        catch {
            $caughtErrors += "Failed to get $mailbox on ${server}:`r`n$($_.Exception.Message)`r`n"
            continue
        }
        if ($oooBefore.AutoReplyState -eq "Disabled") { continue }
        $before += $oooBefore | Select-Object Identity, AutoReplyState, StartTime, EndTime
        if ($oooBefore.EndTime -gt (Get-Date)) {
            for ($i = 1; $i -le 5; $i ++) {
                try {
                    Set-MailboxAutoReplyConfiguration $mailbox.DistinguishedName -AutoReplyState "Disabled" -ErrorAction "Stop"
                    $intermediate += Get-MailboxAutoReplyConfiguration $mailbox.DistinguishedName | Select-Object Identity, AutoReplyState, StartTime, EndTime
                    Set-MailboxAutoReplyConfiguration $mailbox.DistinguishedName -AutoReplyState $oooBefore.AutoReplyState -StartTime $oooBefore.StartTime -EndTime $oooBefore.EndTime -ErrorAction "Stop"
                }
                catch { $caughtErrors += "Failed to set ${mailbox} on ${server}:`r`n$($_.Exception.Message)`r`n" }
                $oooAfter = Get-MailboxAutoReplyConfiguration $mailbox.DistinguishedName
                if ($oooAfter.AutoReplyState -ne "Disabled") { break }
            }
        }
        else { $oooAfter = $oooBefore }
        $after += $oooAfter | Select-Object Identity, AutoReplyState, StartTime, EndTime
    }
}

Get-ChildItem $logDir | Where-Object { $_.CreationTime -lt (Get-Date).AddDays(-60).Date } | Remove-Item
$before | Export-Csv -Encoding UTF8 -NoTypeInformation -Delimiter ";" "$logDir$(Get-Date -Format yy/MM/dd) Before.csv"
$intermediate | Export-Csv -Encoding UTF8 -NoTypeInformation -Delimiter ";" "$logDir$(Get-Date -Format yy/MM/dd) Intermediate.csv"
$after | Export-Csv -Encoding UTF8 -NoTypeInformation -Delimiter ";" "$logDir$(Get-Date -Format yy/MM/dd) After.csv"
$caughtErrors | Out-File -Encoding UTF8 "$logDir$(Get-Date -Format yy/MM/dd) Errors.txt"
$Error | Out-File -Append -Encoding UTF8 "$logDir$(Get-Date -Format yy/MM/dd) Errors.txt"

# Troubleshooting
# Import-Csv "$logDir$(Get-Date -Format yy.MM.dd) Before.csv" -Delimiter ";" | Group-Object AutoReplyState | Select-Object @{Name='AutoReplyState';Expression={$_.Name}},Count
# Import-Csv "$logDir$(Get-Date -Format yy.MM.dd) After.csv" -Delimiter ";" | Group-Object AutoReplyState | Select-Object @{Name='AutoReplyState';Expression={$_.Name}},Count
# Import-Csv "$logDir$(Get-Date -Format yy.MM.dd) Intermediate.csv" -Delimiter ";" | Group-Object Identity | Group-Object Count | Select-Object @{Name='Attempts';Expression={$_.Name}},Count