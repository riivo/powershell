Import-Module ActiveDirectory

# Define OUs and paths
$listsOu = ""
$contactsOu = ""
$ftpPath = "\\server\share"
$usedPath = ""
$logPath = ""

# Remove old files
Get-ChildItem $logPath | Where-Object { $_.CreationTime -lt (Get-Date).AddDays(-90) } | Remove-Item
Get-ChildItem $usedPath | Sort-Object -Descending | Select-Object -Skip 100 | Remove-Item

# Define functions
function New-List ($mail) {
    $params = @{
        Name = $mail
        DisplayName = $mail
        GroupCategory = "Distribution"
        GroupScope = "Universal"
        Path = $listsOu
        OtherAttributes = @{
            mail = "$mail"
            mailNickname = $mail.Split("@")[0]
            proxyAddresses = "SMTP:$mail", "smtp:$($mail.Split("@")[0])@domain.local"
        }
    }
    if (-not (Get-ADObject -Filter { mail -eq $mail })) { New-ADGroup @params }
}
function New-Contact ($mail) {
    $params = @{
        Name = $mail
        DisplayName = $mail
        Type = "Contact"
        Path = $contactsOu
        OtherAttributes = @{
            mail = "$mail"
            mailNickname = $mail.Split("@")[0]
            proxyAddresses = "SMTP:$mail"
            targetAddress = "SMTP:$mail"
        }
        PassThru = $true
    }
    if (-not (Get-ADObject -Filter { mail -eq $mail })) { New-ADObject @params }
}

# Define log file
$logFile = "$logPath\$(Get-Date -Format "yyMMdd_HHmmss").log"
$totals = @{}

# Identify input file
if (-not ($okFilesList = @(Get-ChildItem "$ftpPath\IN\*.OK" | Sort-Object))) {
    "$(Get-Date) No .OK files found" | Out-File -Append $logFile
    exit
}
$inputFile = "$($okFilesList[-1])".TrimEnd(".OK")

# Gather data
"$(Get-Date) Started with input file $inputFile" | Out-file $logFile
$pdmLists = Import-Csv -Delimiter ";" $inputFile |
    Group-Object E_MAIL_LIST_NAME
if (@($pdmLists).Count -lt 2500) {
    "$(Get-Date) Input file has less than 2500 entries" | Out-File -Append $logFile
    exit
}
$adLists = @{}
Get-ADGroup -SearchBase $listsOu -Filter * -Properties Members | 
    ForEach-Object {
        $adLists[$_.Name] = $_.Members
    }
"$(Get-Date) Data gathered" | Out-File -Append $logFile

# Move input files
$okFilesList | ForEach-Object {
    Remove-Item $_
    Move-Item "$_".TrimEnd(".OK") $usedPath
}

# Compare only lists
if ($listDiffs = Compare-Object @($adLists.Keys) @($pdmLists | Select-Object -ExpandProperty Name)) {
    foreach ($listDiff in $listDiffs) {
        $mail = $listDiff.InputObject
        if ($listDiff.SideIndicator -eq "=>") {
            New-List $mail
            $adLists.$mail = @()
            $totals.createdLists++
            "$(Get-Date) Created list '$mail'" | Out-File -Append $logFile
        }
        elseif ($listDiff.SideIndicator -eq "<=") {
            $dn = Get-ADObject -Filter { mail -eq $mail }
            $($adlists.Keys) | ForEach-Object { $adlists.$_ = @($adlists.$_ | Where-Object { $_ -ne $dn }) }
            Remove-ADGroup $mail -Confirm:$false
            $totals.deletedLists++
            "$(Get-Date) Deleted list '$mail'" | Out-File -Append $logFile
        }
    }
}
"$(Get-Date) Lists compared" | Out-File -Append $logFile

# Compare members in each list
foreach ($list in $pdmLists) {
    $listName = $list.Name
    $adMembers = $adLists.$listName | ForEach-Object { (Get-ADObject $_ -Properties mail).mail }
    $pdmMembers = $list.Group | Select-Object -ExpandProperty E_MAIL_LIST_MEMBER
    if ($memberDiffs = Compare-Object @($adMembers | Select-Object) $pdmMembers) {
        foreach ($memberDiff in $memberDiffs) {
            $mail = $memberdiff.InputObject
            if ($memberDiff.SideIndicator -eq "=>") {
                if (-not ($dn = Get-ADObject -Filter { mail -eq $mail })) {
                    $dn = New-Contact $mail
                    "$(Get-Date) Created a new contact for '$mail'" | Out-File -Append $logFile
                }
                Set-ADGroup $listName -Add @{ member = "$dn" }
                $totals.addedMembers++
                "$(Get-Date) Added '$mail' to '$listName'" | Out-File -Append $logFile
            }
            elseif ($memberDiff.SideIndicator -eq "<=") {
                Set-ADGroup $listName -Remove @{ member = "$(Get-ADObject -Filter { mail -eq $mail })" }
                $totals.removedMembers++
                "$(Get-Date) Removed '$mail' from '$listName'" | Out-File -Append $logFile
            }
        }
    }
}

# Remove stale contacts
# Get-ADObject -LDAPFilter '(&(objectclass=contact)(!memberof=*))' -SearchBase $contactsOu | Remove-ADObject -Confirm:$false

# Finish logging
"$(Get-Date) Finished" | Out-File -Append $logFile
$Error | Out-File -Append $logFile

# Create "SUCCESS" file
"Time: {0}
Email lists created: {1}
Email lists deleted: {2}
List members created: {3}
List members deleted: {4}" -f
    "$((Get-Date).ToUniversalTime())",
    [Int]$totals.createdLists,
    [Int]$totals.deletedLists,
    [Int]$totals.addedMembers,
    [Int]$totals.removedMembers |
        Out-File -Append -Encoding UTF8 ($inputFile.Replace("\IN\", "\OUT\") + "_SUCCESS")
"" | Out-File ($inputFile.Replace("\IN\", "\OUT\") + "_SUCCESS.OK")